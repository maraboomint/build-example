Feature: Test a restful api to see how well i can do it

  Scenario: GET request test
    When  I request "GET /notes"
    Then I get a "200" status code
    And the response should be json
    And the api response should have parameter  "data"
    And the api response should contain "Hello World" in parameter "message"
