<?php

use Behat\Behat\Context\ClosuredContextInterface,
    Behat\Behat\Context\TranslatedContextInterface,
    Behat\Behat\Context\BehatContext,
    Behat\Behat\Exception\PendingException;
use Behat\Gherkin\Node\PyStringNode,
    Behat\Gherkin\Node\TableNode;

//
// Require 3rd-party libraries here:
//
//   require_once 'PHPUnit/Autoload.php';
//   require_once 'PHPUnit/Framework/Assert/Functions.php';
//

/**
 * Features context.
 */
class FeatureContext extends \Behat\MinkExtension\Context\MinkContext
{

    protected $client;
    /**
     * @var \Guzzle\Http\Message\Response
     */
    protected $response;

    protected $responseBody;

    /**
     * Initializes context.
     * Every scenario gets its own context object.
     *
     * @param array $parameters context parameters (set them up through behat.yml)
     */
    public function __construct(array $parameters)
    {
        // Initialize your context here

        $this->client = new GuzzleHttp\Client(['base_url' => $parameters['base_url']]);


    }


    /**
     * @Given /^I fill in the guest book$/
     */
    public function iFillInTheGuestBook($name = "John Doe", $message = 'Hello')
    {
        $this->fillField('name', $name);
        $this->fillField('message', $message);
        $this->pressButton('Submit');
    }

    /**
     * @Given /^I fill in the guest book incorrectly$/
     */
    public function iFillInTheGuestBookIncorrectly()
    {
        $this->iFillInTheGuestBook('', '');
    }

    /**
     * @When /^I request "(GET|POST|PUT|DELETE) ([^"]*)"$/
     */
    public function iRequest($method, $path)
    {
        $method = strtolower($method);
        switch ($method) {
            case 'get':
                $this->response = $this->client->get($path);
                break;
        }
    }

    /**
     * @Then /^I get a "([^"]*)" status code$/
     */
    public function iGetAStatusCode($statusCode)
    {
        $responseCode = $this->response->getStatusCode();
        if (!$responseCode === $statusCode) {
            throw new ErrorException("Status code of $responseCode is not equal to the expected status code of $statusCode");
        }
    }

    /**
     * @Given /^the response should be json$/
     */
    public function theResponseShouldBeJson()
    {
        $body = $this->response->getBody(true);
        if (!json_decode($body, true)) {
            throw new ErrorException(" Body was not Json");
        }
    }

    /**
     * @Given /^the api response should have parameter  "([^"]*)"$/
     */
    public function theApiResponseShouldHaveParameter($index)
    {
        $body = $this->getBody();
        if (!isset($body[$index]))
            throw new ErrorException('Property not found');
    }


    /**
     * @Given /^the api response should contain "([^"]*)" in parameter "([^"]*)"$/
     */
    public function theApiResponseShouldContainInParameter($value, $key)
    {
        $body = $this->getBody();

        $this->theApiResponseShouldHaveParameter($key);
        if ($body[$key] !== $value) {
            throw new ErrorException('The value of ' . $key . ' is not' . $value);
        }
    }

    /**
     * @return mixed
     */
    private function getBody()
    {
        $body = json_decode($this->response->getBody(true), true);
        return $body;
    }
}
