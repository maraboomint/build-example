Feature: Guest Book
  In order to track my visitors
  As the site owner
  I should be able to allow users to sign the guest book

  Scenario: User fills out guest book
    Given I am on "guests/create"
    And I fill in the guest book
    Then  I should see "Awesome"
    And I should see "John Doe"

  Scenario: User fills out guest book with invalid data
    Given I am on "guests/create"
    And I fill in the guest book incorrectly
    Then  I should see "Whoops"

