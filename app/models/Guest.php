<?php

/**
 * Created by PhpStorm.
 * User: David Kaguma
 * Date: 10/16/2014
 * Time: 3:48 PM
 */
class Guest extends \Illuminate\Database\Eloquent\Model
{
    protected $fillable = ['name','message'];
} 