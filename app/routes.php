<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function () {
    return View::make('hello');
});

Route::get('guests/create', function () {

    return View::make('guests.create');
});

Route::post('guests/create', function () {

    $input = Input::all();
    $validator = Validator::make($input, ['name' => 'required', 'message' => 'required']);
    if ($validator->fails()) {
        throw new LogicException;
    }
    $guest = Guest::create(Input::all());

    return Redirect::route('guests.index');
});
Route::get('/guests/index', ['as' => 'guests.index', function () {

    $guests = Guest::all();
    return View::make('guests.index')->with(compact('guests'));
}]);

Route::get('notes', function () {

    return Response::json(['data' => [], 'message' => 'Hello World']);
});